#!/usr/bin/env python

from distutils.core import setup

setup(name='zeno_joint_controller',
      version='1.0',
      description='Python multiply utility',
      author='Shehzad Ahmed',
      author_email='shehzad.ahmed@smail.inf.h-brs.de',
      url='https://www.h-brs.de/',
      py_modules = ['zeno_head_configuration', 'zeno_head_motors_interface', 'zeno_head_controller',
      				'zeno_body_controller', 'zeno_body_config', 'zeno_joint_controller', 
      				'zeno_remote_communication_server','zeno_configuration'],
      package_dir={'':'src/zeno_joint_controller'}
    )
