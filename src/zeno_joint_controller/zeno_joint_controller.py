#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
This script provides an implementation of zeno joint controller.
"""
__author__ = 'shehzad ahmed'

import logging
from zeno_body_controller import *
from zeno_head_controller import *

from thread import start_new_thread

from zeno_remote_communication_server import *

from zeno_configuration import zeno_joints

class ZenoJointController():
    def __init__(self):
        self.is_controller_initialized =  False
        #self.logger = logging.getLogger("zeno_joint_controller_logger")
        #self.logger.setLevel(logging.INFO)
        #self.fh = logging.FileHandler('spam.log')
        #self.fh.setLevel(logging.INFO)
        #self.addHandler(self.fh)

        self.remote_communication = ZenoRemoteCommunicationServer()
        self.body_joint_controller = ZenoBodyController()
        self.head_joint_controller = ZenoHeadController()
        self.is_body_controller_running = False
        self.is_head_controller_running = False
        self.is_remote_communication_running = False

        self.initialize()

        print("zeno_joint_controller is now running")

    def initialize(self):
        self.is_body_controller_running = self.body_joint_controller.initialize()
        if (not(self.is_body_controller_running)):
            print("Body controller initialization failed.")
        else:
            print("Body controller initialization successful.")

        self.is_head_controller_running = self.head_joint_controller.initialize()
        if (not(self.is_head_controller_running)):
            print("Head controller initialization failed.")
        else:
            print("Head controller initialization successful.")

        self.is_remote_communication_running = self.remote_communication.initialize()
        if (not(self.is_remote_communication_running)):
            print("ZMQ communication initialization failed.")
        else:
            print("ZMQ communication initialization successful.")

    def execute_cycle(self):
        # read command
        command = self.remote_communication.subscribe_data()

        if command is None:
            return

        print command['op']
        if command['op'] == 'position_command':
            self.set_joint_positions(command['joint_names'],
                                     command['joint_positions'])
        elif command['op'] == 'velocity_command':
            self.set_joint_positions(command['joint_names'],
                                     command['joint_velocities'])
        elif command['op'] == 'joint_states':
            self.get_joint_positions(command['joint_names'])

    def set_joint_positions(self, names, positions):
        print("Moving joints to commanded positions.")
        head_command = {
                        'names': [],
                        'positions': []
                        }

        body_command = {
                        'names': [],
                        'positions': []
                        }

        for idx, joint_name in enumerate(names):
            if joint_name in zeno_joints["head"]:
                head_command['names'].append(joint_name)
                head_command['positions'].append(positions[idx])
            elif joint_name in zeno_joints["body"]:
                body_command['names'].append(joint_name)
                body_command['positions'].append(positions[idx])

        if len(head_command['names']) != 0: 
            self.head_joint_controller.set_joint_position_commands(
                                            head_command['names'], 
                                            head_command['positions']
                                            )

        if len(body_command['names']) != 0: 
            self.body_joint_controller.set_joint_position_commands(
                                            body_command['names'], 
                                            body_command['positions']
                                            )

    def set_joint_velocities(self, joint_names, joint_velocities):
        print("Moving joints to commanded positions.")
        pass

    def get_joint_positions(self, joint_names):
        head_joint_names = []
        head_joint_positions = []
        body_joint_names = []
        body_joint_positions = []

        joint_positions = []

        for joint_name in joint_names:
            if joint_name in zeno_joints["head"]:
                head_joint_names.append(joint_name)
            elif joint_name in zeno_joints["body"]:
                body_joint_names.append(joint_name)

        if len(head_joint_names) != 0: 
            head_joint_positions = self.head_joint_controller.get_current_joint_angles(
                                                head_joint_names
                                            )

        if len(body_joint_names) != 0: 
            body_joint_positions = self.body_joint_controller.get_current_joint_angles(
                                                body_joint_names
                                            )
        for joint_name in joint_names:
            if joint_name in head_joint_names:
                idx = head_joint_names.index(joint_name)
                joint_positions.append(
                                        head_joint_positions[idx]
                                        )
            elif joint_name in body_joint_names:
                idx = body_joint_names.index(joint_name)
                joint_positions.append(
                                        body_joint_positions[idx]
                                        )

        joint_states = {
            'joint_states': joint_positions
        }

        self.remote_communication.publish_data(joint_states)

    def get_joint_velocities(self, joint_names):
        pass

    def __del__(self):
        print("'killing on exit'.")
