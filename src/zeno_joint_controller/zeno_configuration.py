#!/usr/bin/python

communication_config = {
    'robot': {
        #'address': "tcp://localhost:5556"
        'address' : "tcp://192.168.188.25:5556"
    },

    'remote_host': {
        'address': "tcp://*:5557"
    }   
}

zeno_joints = {
    'head': [
            'neck_yaw', 'neck_roll', 'neck_pitch', 'left_eye_yaw', 
            'right_eye_yaw', 'left_smile', 'right_smile',
            'brows', 'jaw', 'eyelids','eyes_pitch'
            ],

    'body': [
            'waist','left_shoulder_pitch', 'left_shoulder_roll',
            'left_elbow_roll', 'left_elbow_yaw',
            'right_shoulder_pitch', 'right_shoulder_roll', 
            'right_elbow_roll', 'right_elbow_yaw',
            'left_hip_roll', 'left_hip_yaw', 'left_hip_pitch',
            'left_knee_pitch', 'left_ankle_pitch', 'left_ankle_roll',
            'right_hip_roll', 'right_hip_yaw', 'right_hip_pitch',
            'right_knee_pitch', 'right_ankle_pitch', 'right_ankle_roll'
            ]
}
