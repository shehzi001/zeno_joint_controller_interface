#!/usr/bin/python
import time
from json import dumps, load
import time
from random import choice
from random import randrange
 
import zmq

from zeno_configuration import communication_config

class ZenoRemoteCommunicationServer():
    def __init__(self):
        self.remote_host_address = communication_config['remote_host']['address']
        self.robot_address = communication_config['robot']['address']
        self.connection_timeout = 1.0
        self.joint_states_pub = None
        self.command_sub = None

    def initialize(self):
        #connect to the socket
        try:
            self.context_joint_states = zmq.Context()
            self.joint_states_pub = self.context_joint_states.socket(zmq.PUB)

            self.context_command = zmq.Context()
            self.command_sub = self.context_command.socket(zmq.SUB)
            self.command_sub.setsockopt(zmq.SUBSCRIBE, '')

            self.joint_states_pub.bind(self.remote_host_address)
            self.command_sub.connect(self.robot_address)
            print 'ZMQ communication initialization.'
            time.sleep(1)
        except:
            self.joint_states_pub = None
            self.command_sub = None
            print 'ZMQ communication initialization failed.'
            return False    

        return True
    
    def publish_data(self, data_in):
        if self.joint_states_pub is None:
            print 'ZMQ communication is not initialized'
            return

        try:
            self.joint_states_pub.send_json(data_in)
        except zmq.error.ZMQError:
            print 'ZMQ error'

    def subscribe_data(self):
        if self.command_sub is None:
            print 'ZMQ communication is not initialized'
            return
        try:
            return self.command_sub.recv_json()
        except zmq.error.ZMQError:
            print 'ZMQ error'
            return None

    def close_remote_communication(self):
        self.joint_states_pub.close()

    def __del__(self):
        self.close_remote_communication()

