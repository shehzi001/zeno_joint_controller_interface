#from __future__ import print_function
 
import time
from random import choice
from random import randrange
from json import dumps
import zmq
 
if __name__ == "__main__":
    joint_names = ['left_shoulder_pitch', 'left_shoulder_roll', 'left_elbow_yaw', 'left_elbow_pitch']
    joint_values = [0.0, 1.0, 2.0, 3.0]
 
    context = zmq.Context()
    socket = context.socket(zmq.PUB)
    socket.bind("tcp://*:5556")

    context = zmq.Context()
    socket_sub = context.socket(zmq.SUB)
    socket_sub.setsockopt(zmq.SUBSCRIBE, '')
    socket_sub.connect("tcp://localhost:5557")
    time.sleep(2)
    counter = 0
    while True:
        time.sleep(2)
        # compose the message
        #msg = "{0} ${1}".format(joint_names, joint_vaules)
        #print("Sending Message: {0}".format(msg))
        # send the message
        #socket.send(msg)
        #socket.send_multipart([joint_names, joint_vaules]

        
        joint_states = {
            "op": "joint_states",
            "joint_names": joint_names
        }

        #print joint_states

        socket.send_json(joint_states)
        counter = counter + 1
        print counter
         
        json_data = socket_sub.recv_json()
        print("{0}".format(json_data['joint_states']))
        #print("{0}".format(json_data['joint_names']))

        '''
        joint_command = {
            "op": "set_joint_positions",
            "joint_names": joint_names,
            "joint_positions": joint_values
        }

        socket.send_json(joint_command)
        counter = counter + 1
        print counter
        '''

    socket.close()
