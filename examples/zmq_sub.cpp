#include <zmq.hpp>
#include <string>
#include <iostream>
#include <json/json.h>

int main()
{
    // set up zeromq context
    zmq::context_t context(1);

    //  Socket to talk to server
    std::cout << "Collecting updates from server..." << std::endl;
    zmq::socket_t subscriber(context, ZMQ_SUB);
    subscriber.connect("tcp://localhost:5556");

    // subscribe to filter
    std::string filter = "";
    subscriber.setsockopt(ZMQ_SUBSCRIBE, filter.c_str(), filter.length());
    
    // set up jsoncpp objects
    Json::Value v;
    Json::Reader reader;
    Json::StyledWriter styledWriter;

    //  Process 100 updates
    long update_nbr=0;
    for(; ; )
    {
        //zmq::message_t envelope;
        zmq::message_t update;
        // receive the envelope message which is used to filter out subscribers
        //subscriber.recv(&envelope);
        // receive the json message and actually do something with it
        subscriber.recv(&update);
        // convert non null terminating cstring into a string
        std::string response = std::string(
                static_cast<const char*>(update.data()),
                update.size());

        // parse json data
        bool parsingSuccessful = reader.parse(response, v);
        if(parsingSuccessful)
        {
            std::cout << update_nbr << v["joint_names"];//styledWriter.write(v) ;
        }
        update_nbr++;
    }
    return 0;
}
