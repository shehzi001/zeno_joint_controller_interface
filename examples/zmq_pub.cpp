#include <zmq.hpp>
#include <string>
#include <iostream>
#include <json/json.h>
#include <time.h>

int main()
{
    //  Prepare our context and command_pub
    zmq::context_t context(1);
    zmq::socket_t command_pub(context, ZMQ_PUB);
    std::string local_host_address("tcp://localhost:5556");
    command_pub.bind(local_host_address.c_str());

    Json::Value command;

    zmq::context_t context1(1);
    zmq::socket_t joint_states_sub(context1, ZMQ_SUB);
    std::string robot_address("tcp://localhost:5557");
    joint_states_sub.connect(robot_address.c_str());
    // subscribe to filter
    std::string filter = "";
    joint_states_sub.setsockopt(ZMQ_SUBSCRIBE, filter.c_str(), filter.length());
    
    // set up jsoncpp objects
    Json::Reader reader;
    Json::StyledWriter styledWriter;

    //sleep(1);

    while(1)
    {
        // Fill message
        Json::Value joint_names, v;
        joint_names[0] = Json::Value("left_hip_roll");
        joint_names[1] = Json::Value("left_hip_pitch");

        // Fill command
        command["op"] = "get_joint_positions";
        command["joint_names"] = joint_names;

        // Prepare Json message
        Json::FastWriter fastWriter;
        std::string jsonMessage = fastWriter.write(command);

        zmq::message_t command_init(jsonMessage.length());
        memcpy(command_init.data(), jsonMessage.c_str(), jsonMessage.length());

        //  Prepare zmq message
        zmq::message_t message(jsonMessage.length());
        memcpy(message.data(), jsonMessage.c_str(), jsonMessage.length());

        //Json::StyledWriter styledWriter;
        //std::cout << styledWriter.write(command);

        std::cout << "sending joint_state command...." << std::endl;
        command_pub.send(message);

        zmq::message_t joint_states;
        std::cout << "waiting for the reply...." << std::endl;
        joint_states_sub.recv(&joint_states);

       // convert non null terminating cstring into a string
        std::string response = std::string(
                static_cast<const char*>(joint_states.data()),
                joint_states.size());

        // parse json data
        bool parsingSuccessful = reader.parse(response, v);
        if(parsingSuccessful)
        {
            std::cout << v["joint_values"];
        }
        //sleep(2);
    }

    return 0;
}
